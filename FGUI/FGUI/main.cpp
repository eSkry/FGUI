


#include <SFML\Graphics.hpp>
#include <FGUI.h>
#include <Button.h>
#include <CheckBox.h>
#include <Window.h>
#include <FGUIHelper.h>

#include <iostream>

int main(){
	setlocale(LC_ALL, "Russian");

	sf::RenderWindow window(sf::VideoMode(1100, 700), "Test FGUI");
	window.setFramerateLimit(60);

	//FGUI::Button bt1(window);
	sf::Texture textureBt;
	textureBt.loadFromFile("./data/bt.png");
	//bt1.setTexture(textureBt);

	sf::Font font;
	font.loadFromFile("./data/ARIAL.TTF");
	/*bt1.setFont(font);
	bt1.setText("Click me");
	bt1.setTextColor(sf::Color::Green);

	bt1.setPosition(500, 500);

	bt1.setOnClickListener([&](size_t id) {
		std::cout << "�����!" << std::endl;
	});

	bt1.setOnCursorInListener([&](size_t id) {
		std::cout << "�����!" << std::endl;
	});

	bt1.setOnCursorLeftListenter([&](size_t id) {
		std::cout << "�����!" << std::endl;
	});

	FGUI::CheckBox cb1(window);
	
	cb1.setPosition(100,100);

	cb1.setOnCheckListener([&](size_t id, bool isChecked) {
		std::cout << "Checked: " << isChecked << std::endl;
	});

	FGUI::Window window1(window);*/

	FGUI::FGUIHelper helper(window);

	FGUI::Button*	bt1 = &helper.createButton();
	bt1->setFont(font);
	bt1->setTexture(textureBt);
	bt1->setOnClickListener([&](size_t id) {
		std::cout << "�����!" << std::endl;
	});

	while (window.isOpen()) {

		sf::Event event;
		while (window.pollEvent(event)) {

			if (event.type == sf::Event::Closed)
				window.close();

			helper.update(event);
			//bt1->update(event);
			//cb1.update(event);
		//	window1.update(event);
		}
		

		window.clear(sf::Color(122,122,122));
		//bt1->draw();
		//cb1.draw();
		//window1.draw();
		helper.draw();
		window.display();
	}



	return 0;
}