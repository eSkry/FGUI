#include <FGUI.h>

namespace FGUI{

FGui::FGui(sf::RenderWindow &window) {
	FGUI_ID = $id();
	this->window = &window;
	this->induced = false;
	
	clickListener = [](size_t id) {};
	inducedListener = [](size_t id) {};
	cursorLeftListener = [](size_t id) {};
	mainShape.setSize(sf::Vector2f(150, 60));
}


FGui::~FGui() {}

void FGui::setOnClickListener(std::function<void(size_t)> listener) {
	this->clickListener = listener;
}

void FGui::setOnCursorInListener(std::function<void(size_t)> listener) {
	this->inducedListener = listener;
}

void FGui::setOnCursorLeftListenter(std::function<void(size_t)> listener) {
	cursorLeftListener = listener;
}

const size_t& FGui::getID() const {
	return FGUI_ID;
}

void FGui::setTexture(sf::Texture & texture) {
	mainShape.setTexture(&texture);
	mainShape.setTextureRect( sf::IntRect(0,0,texture.getSize().x, texture.getSize().y ));
	recalculate();
}

void FGui::setTextureRect(sf::IntRect rect) {
	mainShape.setTextureRect(rect);
	recalculate();
}

void FGui::setPosition(const sf::Vector2f & position) {
	mainShape.setPosition(position);
	recalculate();
}

void FGui::setPosition(float x, float y) {
	mainShape.setPosition(x, y);
	recalculate();
}

void FGui::setScale(float x, float y) {
	mainShape.setScale(x, y);
	recalculate();
}

void FGui::setSize(sf::Vector2f size) {
	mainShape.setSize(size);
	recalculate();
}

void FGui::setColor(sf::Color color) {
	mainShape.setFillColor(color);
}

const sf::Vector2f & FGui::getPosition() const {
	return mainShape.getPosition();
}

const sf::FloatRect FGui::getGlobalBounds() const {
	return mainShape.getGlobalBounds();
}



}