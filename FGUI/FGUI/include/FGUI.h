#ifndef FGUI_H
#define FGUI_H

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

#include <functional>
#include <iostream>	// FOR TEST

#include "idgenerator/id.hpp"

namespace FGUI{

class FGui {
public:
	FGui(sf::RenderWindow &window);
	~FGui();

	//////// SYSTEM
	virtual void update(sf::Event &event) = 0; 
	virtual void draw() = 0;

	//////// SPRITE SETTINGS
	void setTexture(sf::Texture& texture);
	void setTextureRect(sf::IntRect rect);
	void setPosition(const sf::Vector2f& position);
	void setPosition(float x, float y);
	void setScale(float x, float y);
	void setSize(sf::Vector2f size);
	void setColor(sf::Color color);

	const sf::Vector2f& getPosition() const;
	const sf::FloatRect getGlobalBounds() const;

	//////// FGUI SETTINGS
	void setOnClickListener(std::function< void(size_t) > listener);
	void setOnCursorInListener(std::function< void(size_t) > listener);
	void setOnCursorLeftListenter(std::function< void(size_t) > listener);
	const size_t& getID() const;

protected:
	virtual	void recalculate() = 0;	// ������ ������� ������������� ��� ����������� �����������, ����� ������ setTexture, setPosition � �.�.

	sf::RectangleShape					mainShape;			// ��� ������ � ������� ��������������� ������������
	sf::RenderWindow					*window;

	std::function<void(size_t)>			clickListener;
	std::function<void(size_t)>			inducedListener;
	std::function<void(size_t)>			cursorLeftListener;
	size_t								FGUI_ID = 0;			// ID �������� // WARNING ��� ����� ������, ������ ��� ����� ��� ���������
	
	bool								induced;				// true - ������ ������� �� �������

};

}

#endif