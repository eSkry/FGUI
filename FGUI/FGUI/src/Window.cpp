#include <Window.h>

FGUI::Window::Window(sf::RenderWindow & window) : FGui(window) {
	hasFocused = true;
	closeButton = new FGUI::Button(window);

	_titleShape.setSize(sf::Vector2f(600, 26));
	_titleShape.setFillColor(sf::Color(104, 157, 255));
	mainShape.setSize(sf::Vector2f(600, 480));
	mainShape.setFillColor(sf::Color(209, 241, 255));
	_titleBar.setString("Window");

	focusListener = [](size_t, bool) {};
	closeButton->setSize(sf::Vector2f(_titleShape.getSize().y, _titleShape.getSize().y));
	closeButton->setColor(sf::Color(255, 158, 179));

	recalculate();

	closeButton->setOnCursorInListener([&](size_t) {
		closeButton->setColor(sf::Color(255, 56, 99));
	});

	closeButton->setOnCursorLeftListenter([&](size_t) {
		closeButton->setColor(sf::Color(255, 158, 179));
	});

	mmoved = false;
}

FGUI::Window::~Window() {
	delete closeButton;
}

void FGUI::Window::update(sf::Event & event) {
	const sf::Vector2f mousePos = window->mapPixelToCoords(sf::Mouse::getPosition(*window));

	//////////// TITLE BAR //////////// 
	if (_titleShape.getGlobalBounds().contains(mousePos)) {

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			
			
			float dX = mousePos.x - _titleShape.getPosition().x;
			float dY = mousePos.y - _titleShape.getPosition().y;

			_titleShape.setPosition(mousePos.x - dX, mousePos.y - dY);
			recalculate();
		}
	}
	//////////// TITLE BAR //////////// 

	//////////// ���������� ���� ////////////
	if (_windowRect.contains(mousePos)) {
		if (!induced) {
			inducedListener(FGUI_ID);
			induced = true;
		}

		if (event.type == sf::Event::MouseButtonPressed) {
			clickListener(FGUI_ID);
			if (!hasFocused) {
				hasFocused = true;			// ���� ������ � ����
				focusListener(FGUI_ID, hasFocused);
			}
		}

	} else {
		if (event.type == sf::Event::MouseButtonPressed) {
			if (hasFocused) {
				hasFocused = false;	// ���� ������ �� ��������� ����
				focusListener(FGUI_ID, hasFocused);
			}
		}

		if (induced) {
			cursorLeftListener(FGUI_ID);
			induced = false;
		}
	}
	//////////// ���������� ���� ////////////

	closeButton->update(event);

}

void FGUI::Window::draw() {
	window->draw(_titleShape);
	window->draw(_titleBar);
	closeButton->draw();
	window->draw(mainShape);
}

void FGUI::Window::recalculate() {
	sf::FloatRect titleBarBounds = _titleShape.getGlobalBounds();
	sf::FloatRect frameBounds = mainShape.getGlobalBounds();

	_windowRect.left = titleBarBounds.left;
	_windowRect.top = titleBarBounds.top;
	_windowRect.width = titleBarBounds.width;
	_windowRect.height = titleBarBounds.height + frameBounds.height;

	mainShape.setPosition(titleBarBounds.left, _titleShape.getPosition().y + titleBarBounds.height);

	closeButton->setPosition(	titleBarBounds.left + titleBarBounds.width - closeButton->getGlobalBounds().width,
								titleBarBounds.top);
}
