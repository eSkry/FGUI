#include <FGUIHelper.h>

FGUI::FGUIHelper::FGUIHelper(sf::RenderWindow& window) {
	this->window = &window;
}

void FGUI::FGUIHelper::draw() {
	for (auto it = renderWidgetOrder.begin(); it != renderWidgetOrder.end(); it++) {
		widgets[ (*it) ]->draw();
	}
}

void FGUI::FGUIHelper::update(sf::Event & event) {
	for (auto &it = widgets.begin(); it != widgets.end(); it++) {
		it->second->update(event);
	}
}

FGUI::Button & FGUI::FGUIHelper::createButton() {
	FGUI::Button*	temp = new FGUI::Button(*window);
	size_t id = temp->getID();
	renderWidgetOrder.push_back(id);
	widgets.insert( std::pair<size_t, FGUI::FGui*>(id, temp) );
	return *temp;
}
