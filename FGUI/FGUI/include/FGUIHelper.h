#ifndef FGUIHELPER_H
#define FGUIHELPER_H

///// SFML /////
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

///// FGUI /////
#include <FGUI.h>
#include <Button.h>

///// STL /////
#include <vector>
#include <map>

namespace FGUI {

class FGUIHelper {
public:
	FGUIHelper(sf::RenderWindow	&window);

	///// Interface
	void draw();
	void update(sf::Event& event);

	FGUI::Button&	createButton();

private:
	sf::RenderWindow*		window;

	std::map<size_t, FGUI::FGui*>	widgets;
	std::vector<size_t>				renderWidgetOrder;
};

}

#endif