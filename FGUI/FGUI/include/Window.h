#ifndef WINDOW_H
#define WINDOW_H

#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/Text.hpp>

#include <FGUI.h>
#include <Button.h>

namespace FGUI {

class Window : public FGui{
public:
	Window(sf::RenderWindow& window);
	~Window();

	virtual void update(sf::Event& event) override;
	virtual void draw() override;
	
	//// LABLE
	void setTitleText(std::string title);
	void setTitleTextColor(sf::Color color);
	void setTitleFont(sf::Font& font);

	//// TITLE BAR
	void setTitleBarTexture(sf::Texture& texture);
	void setTitleBarColor(sf::Color color);

	//// WINDOW FRAME
	void setWindowFrameColor(sf::Color color);

	//// WINDOW
	void close();

protected:
	virtual void recalculate() override;

	sf::Text				_titleBar;
	sf::RectangleShape		_titleShape;			// ���� ����� �����
	FGUI::Button*			closeButton;			// ������ �������� ����
	sf::FloatRect			_windowRect;			// ������� ����

	size_t					titleBarHeight;			// ������ ����� ����
	bool					hasFocused;				// �������������� �� ����
	bool					mmoved;

	std::function<void(size_t, bool)>				focusListener;
};

}

#endif