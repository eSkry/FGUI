#include <Button.h>

namespace FGUI{

Button::Button(sf::RenderWindow& window) : FGui(window) {
	buttonText.setString("button");
	mainShape.setSize(sf::Vector2f(150, 60));
}


Button::~Button() {}

void Button::update(sf::Event & event) {
	if (mainShape.getGlobalBounds().contains(window->mapPixelToCoords(sf::Mouse::getPosition(*window)))) {
		if (!induced) {
			inducedListener(FGUI_ID);
			induced = true;
		}

		if (event.type == sf::Event::MouseButtonPressed) {
			clickListener(FGUI_ID);
		} else if (event.type == sf::Event::MouseButtonReleased) {

		}
		
	} else {
		if (induced) {
			cursorLeftListener(FGUI_ID);
			induced = false;
		}
	}
}

void Button::draw() {
	window->draw(mainShape);
	window->draw(buttonText);
}

void Button::setFont(sf::Font & font) {
	buttonText.setFont(font);
	recalculate();
}

void Button::setText(std::string label) {
	buttonText.setString(label);
	recalculate();
}

void Button::setTextColor(sf::Color color) {
	buttonText.setFillColor(color);
}

void Button::setTextScale(float x, float y) {
	buttonText.setScale(x, y);
}

void Button::recalculate() {
	buttonText.setOrigin( buttonText.getGlobalBounds().width / 2, buttonText.getGlobalBounds().height / 2 );

	sf::Vector2f spriteCenter = { mainShape.getPosition().x + (mainShape.getGlobalBounds().width /2),
								  mainShape.getPosition().y + (mainShape.getGlobalBounds().height / 2)};

	sf::Vector2f newTextPos = { spriteCenter.x,
								spriteCenter.y - (buttonText.getGlobalBounds().height / 2)};
	
	buttonText.setPosition(newTextPos);
}




}