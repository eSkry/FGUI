#ifndef CHECKBOX_H
#define CHECKBOX_H

#include <SFML/Graphics/RenderWindow.hpp>

#include <FGUI.h>

namespace FGUI {

class CheckBox : public FGui{
public:
	CheckBox(sf::RenderWindow& window);

	void update(sf::Event &event) override;
	void draw() override;

	void setOnCheckListener(std::function< void(size_t, bool) > listener);

	bool isChecked() const;
	void setChecked(bool isChecked);

	void setCheckedTexture(sf::Texture &texture);
	void setCheckedTextureRect(sf::IntRect rect);
	void setCheckedColor(sf::Color color);

protected:
	sf::RectangleShape			checkedShape;
	
	void recalculate() override;

	std::function<void(size_t, bool)>	checkListener;
	
	bool _isChecked;
};

}

#endif