#include <CheckBox.h>

FGUI::CheckBox::CheckBox(sf::RenderWindow & window) : FGui(window) {
	_isChecked = false;
	checkListener = [](size_t, bool) {};

	sf::Vector2f size(20, 20);
	mainShape.setSize(size);
	checkedShape.setSize(size);

	mainShape.setFillColor(sf::Color(244, 66, 104));
	checkedShape.setFillColor(sf::Color(116, 244, 65));
}

void FGUI::CheckBox::update(sf::Event & event) {
	if (mainShape.getGlobalBounds().contains(window->mapPixelToCoords(sf::Mouse::getPosition(*window)))) {
		if (!induced) {
			inducedListener(FGUI_ID);
			induced = true;
		}

		if (event.type == sf::Event::MouseButtonPressed) {
			clickListener(FGUI_ID);
			_isChecked = !_isChecked;
			checkListener(FGUI_ID, _isChecked);
		} else if (event.type == sf::Event::MouseButtonReleased) {

		}

	} else {
		if (induced) {
			cursorLeftListener(FGUI_ID);
			induced = false;
		}
	}
}

void FGUI::CheckBox::draw() {
	window->draw(mainShape);
	if (_isChecked) window->draw(checkedShape);
}

void FGUI::CheckBox::setOnCheckListener(std::function<void(size_t, bool)> listener) {
	checkListener = listener;
}

bool FGUI::CheckBox::isChecked() const {
	return _isChecked;
}

void FGUI::CheckBox::setChecked(bool isChecked) {
	this->_isChecked = isChecked;
}

void FGUI::CheckBox::setCheckedTexture(sf::Texture &texture) {
	checkedShape.setTexture(&texture);
	checkedShape.setTextureRect(sf::IntRect(0, 0, texture.getSize().x, texture.getSize().y));
	recalculate();
}

void FGUI::CheckBox::setCheckedTextureRect(sf::IntRect rect) {
	checkedShape.setTextureRect(rect);
	recalculate();
}

void FGUI::CheckBox::setCheckedColor(sf::Color color) {
	checkedShape.setFillColor(color);
}

void FGUI::CheckBox::recalculate() {
	checkedShape.setPosition(mainShape.getPosition());
}
