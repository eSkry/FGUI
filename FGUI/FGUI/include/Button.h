#ifndef BUTTON_H
#define BUTTON_H

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Color.hpp>
#include <string>

#include "FGUI.h"

namespace FGUI{

class Button : public FGui {
public:
	Button(sf::RenderWindow& window);
	~Button();

	void update(sf::Event& event) override;
	void draw() override;

	void setFont(sf::Font& font);
	void setText(std::string label);
	void setTextColor(sf::Color color);
	void setTextScale(float x, float y);

protected:

	void recalculate();

private:
	sf::Text		buttonText;

};

}

#endif